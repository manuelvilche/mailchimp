<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_Campaign extends Mailchimp
{
    const urlGetCampaigns           = "/campaigns";
    const urlCampaignById           = "/campaigns/{campaing_id}";
    const urlCampaignAction         = "/campaigns/{campaing_id}/actions/{action}";
    const urlCampaignContent        = "/campaigns/{campaing_id}/content";
    const urlCampaignFeedback       = "/campaigns/{campaing_id}/feedback";

    /**
     * Gets the campaigns.
     *
     * @return Int The campaigns.
     */
    public function getCampaigns()
    {
        return $this->get($this::urlGetCampaigns);
    }

    /**
     * Gets the campaing.
     *
     * @param Int $campaingId The campaing identifier
     *
     * @return Array The campaing.
     */
    public function getById($campaingId)
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignById);

        return $this->get($url);
    }

    /**
     * Creates a campaing.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlGetCampaigns;

        return $this->post($url, $data);
    }

    /**
     * Update a campaing
     *
     * @param int $campaingId The campaing identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($campaingId, $data = array())
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a campaing
     *
     * @param int $campaingId The campaing identifier
     *
     * @return array The result of the delete
     */
    public function delete($campaingId)
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignById);

        return $this->delete($url, $data);
    }

    /**
     * Makes an action.
     *
     * @param string $actionName The action name
     * @param int $campaingId The campaing identifier
     * @param array $data The data
     *
     * @return array The result of the action
     */
    public function makeAction($actionName, $campaingId, $data = array())
    {
        if($action == "send") $action = "send";
        elseif($action == "cancel") $action = "cancel-send";
        elseif($action == "resume") $action = "resume";
        elseif($action == "pause") $action = "pause";
        elseif($action == "replicate") $action = "replicate";
        elseif($action == "schedule") $action = "schedule";
        elseif($action == "unschedule") $action = "unschedule";
        elseif($action == "test") $action = "test";
        else return false;

        $url = str_replace(array("{campaing_id}", "{action}"), array($campaingId, $action), $this::urlCampaignAction);

        return $this->post($url, $data);
    }

    /**
     * Gets the content.
     *
     * @param int $campaingId The campaing identifier
     *
     * @return array The content.
     */
    public function getContent($campaingId)
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignContent);

        return $this->get($url);
    }

    /**
     * Update the content
     *
     * @param int $campaingId The campaing identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function updateContet($campaingId, $data = array())
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignContent);

        return $this->post($url);
    }

    /**
     * Gets the feedback.
     *
     * @param int $campaingId The campaing identifier
     *
     * @return array The feedback.
     */
    public function getFeedback($campaingId)
    {
        $url = str_replace("{campaing_id}", $campaingId, $this::urlCampaignFeedback);

        return $this->get($url);
    }

}