<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_Campaing_Folder extends Mailchimp
{
    const urlGetCampaingFolders       = "/campaign-folders";
    const urlGetCampaingFolderById    = "/campaign-folders/{folder_id}";

    /**
     * Gets the campaign folders.
     *
     * @return Int The campaign folders.
     */
    public function getCampaingFolders()
    {
        return $this->get($this::urlGetCampaingFolders);
    }

    /**
     * Gets the campaign folders.
     *
     * @param Int $folderId The campaign folders identifier
     *
     * @return Array The campaign folders.
     */
    public function getById($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetCampaingFolderById);

        return $this->get($url);
    }

    /**
     * Creates a campaign folders.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlGetCampaingFolders;

        return $this->post($url, $data);
    }

    /**
     * Update a campaign folders
     *
     * @param int $folderId The campaign folders identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($folderId, $data = array())
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetCampaingFolderById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a campaign folders
     *
     * @param int $folderId The campaign folders identifier
     *
     * @return array The result of the delete
     */
    public function delete($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetCampaingFolderById);

        return $this->delete($url, $data);
    }

}