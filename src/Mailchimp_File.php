<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_File extends Mailchimp
{
    const urlFiles      = "/file-manager/files";
    const urlFileById   = "/file-manager/files/{file_id}";

    /**
     * Gets the files.
     *
     * @return Int The files.
     */
    public function getFiles()
    {
        return $this->get($this::urlFiles);
    }

    /**
     * Gets the file.
     *
     * @param Int $fileId The file identifier
     *
     * @return Array The file.
     */
    public function getById($fileId)
    {
        $url = str_replace("{file_id}", $fileId, $this::urlFileById);

        return $this->get($url);
    }

    /**
     * Creates a file.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlFiles;

        return $this->post($url, $data);
    }

    /**
     * Update a file
     *
     * @param int $fileId The file identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($fileId, $data = array())
    {
        $url = str_replace("{file_id}", $fileId, $this::urlFileById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a file
     *
     * @param int $fileId The file identifier
     *
     * @return array The result of the delete
     */
    public function delete($fileId)
    {
        $url = str_replace("{file_id}", $fileId, $this::urlFileById);

        return $this->delete($url, $data);
    }

}