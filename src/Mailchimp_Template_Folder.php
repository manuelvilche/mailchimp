<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_Template_Folder extends Mailchimp
{
    const urlGetTemplateFolders       = "/template-folders";
    const urlGetTemplateFolderById    = "/template-folders/{folder_id}";

    /**
     * Gets the template folders.
     *
     * @return Int The template folders.
     */
    public function getTemplateFolders()
    {
        return $this->get($this::urlGetTemplateFolders);
    }

    /**
     * Gets the template folders.
     *
     * @param Int $folderId The template folders identifier
     *
     * @return Array The template folders.
     */
    public function getById($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetTemplateFolderById);

        return $this->get($url);
    }

    /**
     * Creates a template folders.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlGetTemplateFolders;

        return $this->post($url, $data);
    }

    /**
     * Update a template folders
     *
     * @param int $folderId The template folders identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($folderId, $data = array())
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetTemplateFolderById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a template folders
     *
     * @param int $folderId The template folders identifier
     *
     * @return array The result of the delete
     */
    public function delete($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetTemplateFolderById);

        return $this->delete($url, $data);
    }

}