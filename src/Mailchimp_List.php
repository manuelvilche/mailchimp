<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_List extends Mailchimp
{
    const urlLists                      = "/lists";
    const urlListById                   = "/lists/{list_id}";
    const urlGetListMembers             = "/lists/{list_id}/members";
    const urlGetListMembersById         = "/lists/{list_id}/members/{subscriber_hash}";
    const urlGetListMembersByIdActivity = "/lists/{list_id}/members/{subscriber_hash}/activity";
    const urlGetListMembersByIdGoals    = "/lists/{list_id}/members/{subscriber_hash}/goals";
    const urlGetListMembersByIdNotes    = "/lists/{list_id}/members/{subscriber_hash}/notes";
    const urlSearchMembers              = "/search-members";
    const urlDeletMember                = "/lists/{list_id}/members/{subscriber_hash}";

    /**
     * Gets the lists.
     *
     * @return Int The lists.
     */
    public function getLists()
    {
        return $this->get($this::urlLists);
    }

    /**
     * Gets the list.
     *
     * @param Int $listId The list identifier
     *
     * @return Array The list.
     */
    public function getById($listId)
    {
        $url = str_replace("{list_id}", $listId, $this::urlListById);

        return $this->get($url);
    }

    /**
     * Creates a list.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlLists;

        return $this->post($url, $data);
    }

    /**
     * Update a list
     *
     * @param int $listId The list identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($listId, $data = array())
    {
        $url = str_replace("{list_id}", $listId, $this::urlListById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a list
     *
     * @param int $listId The list identifier
     *
     * @return array The result of the delete
     */
    public function delete($listId)
    {
        $url = str_replace("{list_id}", $listId, $this::urlListById);

        return $this->delete($url, $data);
    }

    /**
     * Gets the list members.
     *
     * @param Int $listId The list identifier
     *
     * @return Array The list members.
     */
    public function getListMembers($listId)
    {
        $url = str_replace("{list_id}", $listId, $this::urlGetListMembers);

        return $this->get($url);
    }

    /**
     * Gets the list members by identifier.
     *
     * @param Int $listId The list identifier
     * @param String $subscriberHash The subscriber hash
     *
     * @return Array The list members by identifier.
     */
    public function getListMembersById($listId, $subscriberHash)
    {
        $url = str_replace(array("{list_id}", "{subscriber_hash}"), array($listId, md5(strtolower($subscriberHash))), $this::urlGetListMembersById);

        return $this->get($url);
    }

    /**
     * Gets the list members by identifier activity.
     *
     * @param Int $listId The list identifier
     * @param String $subscriberHash The subscriber hash
     *
     * @return Array The list members by identifier activity.
     */
    public function getListMembersByIdActivity($listId, $subscriberHash)
    {
        $url = str_replace(array("{list_id}", "{subscriber_hash}"), array($listId, md5(strtolower($subscriberHash))), $this::urlGetListMembersByIdActivity);

        return $this->get($url);
    }

    /**
     * Gets the list members by identifier goals.
     *
     * @param Int $listId The list identifier
     * @param String $subscriberHash The subscriber hash
     *
     * @return Array The list members by identifier goals.
     */
    public function getListMembersByIdGoals($listId, $subscriberHash)
    {
        $url = str_replace(array("{list_id}", "{subscriber_hash}"), array($listId, md5(strtolower($subscriberHash))), $this::urlGetListMembersByIdGoals);

        return $this->get($url);
    }

    /**
     * Search for members
     *
     * @param String $search The search
     * @param array $params The parameters
     *
     * @return array The result of the search
     */
    public function searchMembers($search, $params = array())
    {
        $url = str_replace(array("{list_id}", "{subscriber_hash}"), array($listId, md5(strtolower($subscriberHash))), $this::urlGetListMembersByIdNotes);

        return $this->get($url);
    }

}