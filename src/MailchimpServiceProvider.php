<?php

namespace Manuelvilche\Mailchimp;

use Illuminate\Support\ServiceProvider;

class MailchimpServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        $configPath = __DIR__ . '/../config/mailchimp.php';
        $resul = $this->publishes([$configPath => $this->getConfigPath()], 'config');
    }

    public function register()
    {
        $configPath = __DIR__ . '/../config/mailchimp.php';
        $this->mergeConfigFrom($configPath, 'mailchimp');
    }

    /**
     * Get the config path
     *
     * @return string
     */
    protected function getConfigPath()
    {
        return config_path('mailchimp.php');
    }
}