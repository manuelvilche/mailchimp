<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_Template extends Mailchimp
{
    const urlTemplates      = "/templates";
    const urlTemplateById   = "/templates/{template_id}";

    /**
     * Gets the templates.
     *
     * @return Int The templates.
     */
    public function getTemplates()
    {
        return $this->get($this::urlTemplates);
    }

    /**
     * Gets the template.
     *
     * @param Int $templateId The template identifier
     *
     * @return Array The template.
     */
    public function getById($templateId)
    {
        $url = str_replace("{template_id}", $templateId, $this::urlTemplateById);

        return $this->get($url);
    }

    /**
     * Creates a template.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlTemplates;

        return $this->post($url, $data);
    }

    /**
     * Update a template
     *
     * @param int $templateId The template identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($templateId, $data = array())
    {
        $url = str_replace("{template_id}", $templateId, $this::urlTemplateById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a template
     *
     * @param int $templateId The template identifier
     *
     * @return array The result of the delete
     */
    public function delete($templateId)
    {
        $url = str_replace("{template_id}", $templateId, $this::urlTemplateById);

        return $this->delete($url, $data);
    }

}