<?php

namespace Manuelvilche\Mailchimp;

class Mailchimp_File_Folder extends Mailchimp
{
    const urlGetFileFolders       = "/file-manager/files";
    const urlGetFileFolderById    = "/file-manager/files/{folder_id}";

    /**
     * Gets the file folders.
     *
     * @return Int The file folders.
     */
    public function getFileFolders()
    {
        return $this->get($this::urlGetFileFolders);
    }

    /**
     * Gets the file folders.
     *
     * @param Int $folderId The file folders identifier
     *
     * @return Array The file folders.
     */
    public function getById($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetFileFolderById);

        return $this->get($url);
    }

    /**
     * Creates a file folders.
     *
     * @param array $data The data
     *
     * @return array The result of the query
     */
    public function create($data = array())
    {
        $url = $this::urlGetFileFolders;

        return $this->post($url, $data);
    }

    /**
     * Update a file folders
     *
     * @param int $folderId The file folders identifier
     * @param array $data The data
     *
     * @return array The result of the update
     */
    public function update($folderId, $data = array())
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetFileFolderById);

        return $this->patch($url, $data);
    }

    /**
     * Delete a file folders
     *
     * @param int $folderId The file folders identifier
     *
     * @return array The result of the delete
     */
    public function delete($folderId)
    {
        $url = str_replace("{folder_id}", $folderId, $this::urlGetFileFolderById);

        return $this->delete($url, $data);
    }

}